﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Sample.DataAccess.Oracle.Configuration;
using SampleBusinessObjects;
using Sample.DataAccess.Core;

namespace Sample.DataAccess.Test
{
    [TestFixture]
    public class DataAccess_StressTests
    {

        [SetUp]
        public void Init()
        {
            Config.Initialize();

            //Create initial records in DB
            DbHelper.Save(GetInitialRecords());
        }

        //Remove all test records from DB
        [TearDown]
        public void Close()
        {
            var cust = DbHelper.RetrieveAll<Customer>();

            if (cust != null)
            {
                DbHelper.Delete<Customer>(cust);
            }
        }

        [Test]
        public void GetRecordsOnCondition_Test()
        {
            // filter criteria
            var name = "Brad";

            // Filter paramiters
            var settings = new Dictionary<string, object>();
            settings["Name"] = name;

            // Get records by filter condition
            var items = DbHelper.RetrieveEquals<Customer>(settings);

            // Check
            Assert.IsNotNull(items, "Should be null");
            Assert.IsTrue(items.Count == 1, "Empty list was received");
            Assert.IsTrue(items[0].Name == name, string.Format("Entity with name '{0}' was not received", name));
        }

        [Test]
        public void GetAllRecordsWithGivenType_Test()
        {
            var items = DbHelper.RetrieveAll<Customer>();

            Assert.IsTrue(items != null && items.Count > 0, "Was not retrieved all the records");
        }

        public void UpdateRecords_Test()
        {
            var updateValue = "(+48)";

            //G\ Getting records from DB
            var customers = DbHelper.RetrieveAll<Customer>();
            Assert.NotNull(customers, "Error in getting data from DB");
            foreach (var customer in customers)
            {// Update records with new value
                customer.TelephoneNumber = updateValue + customer.TelephoneNumber;
            }

            // Save/Update
            DbHelper.SaveOrUpdate<Customer>(customers.ToList());

            // Check
            var updatedItems = DbHelper.RetrieveAll<Customer>();
            Assert.NotNull(updatedItems, "Error in getting data from DB");
            updatedItems.ToList().ForEach(item =>
            {
                // Item should not be null
                Assert.NotNull(item, "Null item error. Check mapping");

                // Check the updated previously value
                Assert.IsNotNullOrEmpty(item.TelephoneNumber, "The value should be null/empty");
                Assert.IsTrue(item.TelephoneNumber.StartsWith(updateValue), "Value was not updated properly");
            });
        }

        private static List<Customer> GetInitialRecords()
        {
            var customers = new List<Customer>(new[] {
            new Customer() { Address = "Street Name 1", Name = "Brad", Surname = "Pit", TelephoneNumber = "54326545" },
            new Customer() { Address = "Street Name 2", Name = "Angelina", Surname = "Julia", TelephoneNumber = "4685468546" }
            });
            return customers;
        }
    }
}
