﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.BusinessObjects
{
    [Serializable]
    public class BaseObject/* : INotifyPropertyChanged*/
    {
        public virtual int Id { get; set; }

        private DateTime _created;
        public virtual DateTime Created
        {
            get { return _created; }
            set { _created = value; }
        }

        private string _createdBy;
        public virtual string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private DateTime _updated;
        public virtual DateTime Updated
        {
            get { return _updated; }
            set { _updated = value; }
        }

        private string _updatedBy;
        public virtual string UpdatedBy
        {
            get { return _updatedBy; }
            set { _updatedBy = value; }
        }

        //public virtual bool IsNew()
        //{
        //    return false;
        //}

        //#region INotifyPropertyChanged implementation
        //public virtual event PropertyChangedEventHandler PropertyChanged;

        //protected virtual void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler tmp = PropertyChanged;
        //    if (tmp != null)
        //    {
        //        tmp(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
        //#endregion

    }
}
