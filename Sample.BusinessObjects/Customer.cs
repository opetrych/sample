﻿using Sample.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleBusinessObjects
{
    [Serializable]
    public class Customer : BaseObject
    {
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual string TelephoneNumber { get; set; }
        public virtual string Address { get; set; }
    }
}
