﻿using Sample.DataAccess.Core;
using Sample.DataAccess.Oracle.Configuration;
using SampleBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Config.Initialize();

            var newRecords = CreateRecords();

            SaveRecords_Test(newRecords);

            //Get records on condotion
            GetRecordsOnCondition_Test();

            // Get all customers
            var customers = GetAllCustomers_Test();

            UpdateCustomers_Test(customers.ToList());

            // Delete all customers
            DeleteAllRecords_Test(customers);

            Console.WriteLine("All the methods run. Press any key to close the process");
            Console.ReadLine();
        }


        private static void SaveRecords_Test(List<Customer> newRecords)
        {
            DbHelper.Save(newRecords);
        }

        private static void UpdateCustomers_Test(IList<Customer> customers)
        {
            var updateValue = "(+48)";
            foreach (var customer in customers)
            {
                customer.TelephoneNumber = updateValue + customer.TelephoneNumber;
            }

            // Save/Update
            DbHelper.SaveOrUpdate<Customer>(customers.ToList());

            // Check
            var updatedItems = DbHelper.RetrieveAll<Customer>();
            foreach (var item in updatedItems)
            {
                if (!string.IsNullOrEmpty(item.TelephoneNumber) && item.TelephoneNumber.StartsWith(updateValue))
                {
                    Console.WriteLine(string.Format("Item '{0}' was updated", item.Name));
                }
                else
                {
                    Console.WriteLine(string.Format("Error: Item '{0}' was NOT updated", item.Name));
                }
            }
        }

        private static void DeleteAllRecords_Test(IList<Customer> customers)
        {
            DbHelper.Delete<Customer>(customers.ToList());

            var list = DbHelper.RetrieveAll<Customer>();
            if (list != null && list.Count == 0)
            {
                Console.WriteLine("All the records was cusseccfully removed");
            }
            else
            {
                Console.WriteLine("Error: records was not removed from DB");
            }
        }

        private static IList<Customer> GetAllCustomers_Test()
        {
            var items = DbHelper.RetrieveAll<Customer>();

            if (items != null)
            {
                if (items.Count == 2)
                {
                    Console.WriteLine(string.Format("Was retried all '2' the records", items.Count));
                }
                else
                {
                    Console.WriteLine("Error: was not retrieved all the records");
                }
            }
            else
            {
                Console.WriteLine("Error: was not retrieved all Customers");
            }

            return items;
        }

        private static void GetRecordsOnCondition_Test()
        {
            var name = "Brad";

            var settings = new Dictionary<string, object>();
            settings["Name"] = name;

            var items = DbHelper.RetrieveEquals<Customer>(settings);

            if (items != null)
            {
                if (items.Count == 1)
                {
                    if (items[0].Name == name)
                    {
                        Console.WriteLine(string.Format("The Entity by filter name '{0}' was received", name));
                    }
                }
                else
                {
                    // Possibly, records was not saved before
                    Console.WriteLine("Error: Enpty list was NOT received. ");
                }
            }
            else
            {
                Console.WriteLine(string.Format("Error: Was not retrieved the records with given condition 'Name' = {0}", name));
            }
        }

        private static List<Customer> CreateRecords()
        {
            var customers = new List<Customer>(new[] {
            new Customer() { Address = "Street Name 1", Name = "Brad", Surname = "Pit", TelephoneNumber = "54326545" },
            new Customer() { Address = "Street Name 2", Name = "Angelina", Surname = "Julia", TelephoneNumber = "4685468546" }
            });
            return customers;
        }
    }
}
