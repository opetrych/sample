﻿using NHibernate;
using NHibernate.Cfg;
using Sample.BusinessObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DataAccess.Core
{
    public static class ConnectionFactory
    {
        private static Dictionary<DataSource, ISessionFactory> _sessionFactories = null;
        private static readonly object _sessionFactoriesSync = new object();

        public static ISessionFactory GetSessionFactory()
        {
            return GetSessionFactory(DataSource.Oracle);
        }

        public static ISessionFactory GetSessionFactory(DataSource dataSource)
        {
            lock (_sessionFactoriesSync)
            {
                return _sessionFactories[dataSource];
            }
        }

        public static void SetSessionFactory()
        {
            SetSessionFactory(DataSource.Oracle);
        }

        public static void SetSessionFactory(DataSource dataSource)
        {
            try
            {
                if (_sessionFactories == null)
                {
                    lock (_sessionFactoriesSync)
                    {
                        if (_sessionFactories == null)
                        {
                            Dictionary<DataSource, ISessionFactory> sessionFactories = new Dictionary<DataSource, ISessionFactory>();

                            sessionFactories[DataSource.Oracle] = ConfigureSession(DataSource.Oracle);

                            _sessionFactories = sessionFactories;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while creating session", ex);
            }
        }

        private static ISessionFactory ConfigureSession(DataSource dataSource)
        {
            Configuration cfg = new Configuration();
            cfg.Configure();
            return cfg.BuildSessionFactory();
        }

    }
}
