﻿using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DataAccess.Core
{
    public class DbHelper
    {
        public static ISessionFactory SessionFactory { get { return ConnectionFactory.GetSessionFactory(); } }

        public static IStatelessSession GetStatelessSession()
        {
            return SessionFactory.OpenStatelessSession();
        }

        public static ISession GetSession()
        {
            return SessionFactory.OpenSession();
        }

        public static void SaveOrUpdate<T>(IEnumerable<T> items)
        {
            try
            {
                using (ISession session = GetSession())
                {
                    foreach (var item in items)
                    {
                        SaveOrUpdate(item, session);
                    }

                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error: entity was not saved/updated", ex);
            }
        }

        public static void SaveOrUpdate<T>(T item)
        {
            try
            {
                using (ISession session = GetSession())
                {
                    SaveOrUpdate(item, session);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error: entity was not saved/updated", ex);
            }
        }

        public static void SaveOrUpdate<T>(T item, ISession session)
        {
            try
            {
                session.SaveOrUpdate(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Save<T>(List<T> items)
        {
            foreach (var item in items)
            {
                Save(item);
            }
        }

        public static void Save<T>(T item)
        {
            try
            {
                using (ISession session = GetSession())
                {
                    session.Save(item);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while saving the entity", ex);
            }
        }

        public static void Delete<T>(T item)
        {
            try
            {
                using (IStatelessSession session = GetStatelessSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.Delete(item);
                        tx.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while deleting the entity", ex);
            }
        }

        public static void Delete<T>(IEnumerable<T> items)
        {
            try
            {
                using (IStatelessSession session = GetStatelessSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        Delete(items, session);
                        tx.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while deleting the entity", ex);
            }
        }

        public static void Delete<T>(IEnumerable<T> items, IStatelessSession session)
        {
            try
            {
                foreach (T item in items)
                {
                    session.Delete(item);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error");
            }
        }

        public static IList<T> RetrieveAll<T>()
        {
            using (ISession session = GetSession())
            {
                return RetrieveAll<T>(session);
            }
        }

        /// <summary>
        /// we can use it within transaction
        /// Retrieve all objects of the type passed in
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session"></param>
        /// <returns></returns>
        public static IList<T> RetrieveAll<T>(ISession session)
        {
            ICriteria targetObjects = session.CreateCriteria(typeof(T));
            return targetObjects.List<T>();
        }

        public static IList<T> RetrieveEquals<T>(IDictionary<string, object> parameters)
        {
            using (ISession session = GetSession())
            {
                return RetrieveEquals<T>(session, parameters);
            }
        }

        public static IList<T> RetrieveEquals<T>(ISession session, IDictionary<string, object> parameters)
        {
            Type type = typeof(T);
            ICriteria criteria = session.CreateCriteria(type);

            if (parameters != null)
            {
                foreach (string key in parameters.Keys)
                {
                    criteria.Add(Restrictions.Eq(key, GetCorrectValue(type, key, parameters[key])));
                }
            }

            // Get the matching objects

            return criteria.List<T>();
        }

        private static object GetCorrectValue(Type type, string propertyName, object value)
        {
            var pi = type.GetProperty(propertyName);
            if (pi != null)
            {
                if (pi.PropertyType == typeof(Int32))
                {
                    checked
                    {
                        return (Int32)Convert.ToInt32(value);
                    }
                }
                else if (pi.PropertyType == typeof(Int64))
                {
                    return (Int64)Convert.ToInt64(value);
                }
            }
            return value;
        }

    }
}
