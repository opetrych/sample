﻿using Sample.DataAccess.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.DataAccess.Oracle.Configuration
{
    public class Config
    {
        public static void Initialize()
        {
            ConnectionFactory.SetSessionFactory();
        }
    }
}
